#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
import os, sys, inspect

scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

scriptname	= os.path.basename(inspect.getfile(inspect.currentframe()))

if 'include' == os.path.basename(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))):
    scriptpath = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
else:
    scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
includepath	= scriptpath + '/include'
if os.path.isdir(includepath):
    sys.path.insert(0, includepath)
#else:
#    print("No include path found!")
#    sys.exit(23)
################################################################################
##from pudb import set_trace; set_trace()
################################################################################
import shutil, time, datetime, locale, logging, logging.handlers, argparse
##import stuff_helper 
##from stuff_helper import myfunc
################################################################################
log = logging.getLogger()
log.setLevel(logging.INFO)
logformatter_main = logging.Formatter(
    '%(asctime)s.%(msecs)03d ' +
    '%(levelname)-8s ' + '%(filename)-16s ' +
    '%(funcName)-12s ' + '%(lineno)04d ' +
    '%(message)s',
    '%Y%m%d%H%M%S')
logformatter_syslog = logging.Formatter(
    '%(levelname)-8s ' +
    '%(filename)-16s ' + '%(funcName)-12s ' +
    '%(lineno)04d ' + '%(message)s')
logcons = logging.StreamHandler()
logcons.setLevel(logging.DEBUG)
logcons.setFormatter(logformatter_main)
log.addHandler(logcons)

#logfile = logging.FileHandler(scriptpath+'/'+scriptname+'.log')
#logfile.setLevel(logging.DEBUG)
#logfile.setFormatter(logformatter_main)
#log.addHandler(logfile)

#logsys = logging.handlers.SysLogHandler(address = '/dev/log')
#logsys.setLevel(logging.INFO)
#logsys.setFormatter(logformatter_syslog)
#log.addHandler(logsys)

################################################################################

parser = argparse.ArgumentParser(description=scriptname + ' commandline parameters.')
parser.add_argument(
    "-c", "--config", dest="config_file",
    action="store", default=scriptpath+"/config/config.json",
    metavar="FILE", help="Use configuration from FILE")
parser.add_argument(
    "-d", "--debug", dest="debug",
    action="store_true", default=False,
    help="show heavy masses(!!) of debug output.")
parser.add_argument(
    "-t", "--test", dest="test",
    action="store_true", default=False,
    help="for testing environment")
cmdline = parser.parse_args()

################################################################################
if cmdline.debug:
    log.setLevel(logging.DEBUG)

################################################################################
log.debug("python version "        + str(sys.version).replace('\n', ''))
log.debug(scriptname+" START "     + '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now()))
log.debug("cmdline.config_file = " + str(cmdline.config_file))
log.debug("cmdline.debug = "       + str(cmdline.debug))
log.debug("cmdline.test = "        + str(cmdline.test))

# your space here

log.debug("program end...")
sys.exit()

