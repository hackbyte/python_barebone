# python_barebone

A script snippet which initializes some basic stuff like:

 Get info about our exe path (where is the script?)
 Checking for an include/ dir.
 Adding the include dir to the python path.
 Importing additional modules (even your own from include/).
 Setup verbose log outputs with python logging.
 Parse cmdline parameters/options.
 Enable verbose debug outputs if -d/--debug was found.
 Show what all that does.

 And, leave room for whatever you wanna do. ;)
